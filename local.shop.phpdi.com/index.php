<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

require __DIR__ . '/vendor/autoload.php';





// ================ Twig ==============
$loader = new FilesystemLoader('templates');
$view =new Environment($loader);
//============== router Slim ================================
$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) use ($view) {
    $body = $view->render('index.twig');
    $response->getBody()->write($body);
    return $response;
});
$app->get('/about', function (Request $request, Response $response, $args) use ($view){
    $body =$view->render('about.twig');
    $response->getBody()->write($body);
    return $response;
});
$app->run();

$item = $_GET['item'];
//создаю контейнер
$app1 = new DI\Container();

//создаю  объект класса User со всеми зависимостями
$user = $app1->get("\App\Classes\User");
$user->setShopping($item);
$user->setShopping("qwerty2");
$user->setShopping("qwerty3");
$user->getShopping();
var_dump($user);
echo "<br> ";
print_r($user);
