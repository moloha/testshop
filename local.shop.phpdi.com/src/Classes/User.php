<?php


namespace App\Classes;



use App\Classes\Services\Basket;

class User
{
    public $basket = [];

    public function __construct(Basket $items)
    {
        $this->basket = $items;
    }

    public function setShopping($item)
    {

        $this->basket-> setItems($item);
    }

    public function getShopping()
    {
        $this->basket->getItems();
    }

}